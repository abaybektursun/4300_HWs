//******************************************************************************
// DESCRIPTIiON:
//   This program calculates sin(x) using maclaurin series expansion 
//   ORDER defines the accuracy (order of the series)
// AUTHORS: Abay Bektursun and Brea Lockhard
//******************************************************************************
#include <stdio.h>
#include <cstdlib>
#include <pthread.h>

#define _USE_MATH_DEFINES
#include <math.h>

#include <cassert>

#define NUM_THREADS 3
#define ORDER 8

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

// Arguments to be passed to threads
struct arg_struct {
    int start;
    int end;
    float x;
};

float result = 0.0;

// Mutual exclusion for atomic sum
pthread_mutex_t mutex1;

long factorial(int n)
{
    long fact = 1;
    for (int c = 1; c <= n; c++)
    {
        fact = fact * c;
        assert(fact > 0);
    }
    return fact;
}

void *sin_mcln(void *args_in)
{
    struct arg_struct *args = (struct arg_struct *)args_in;
    float partial_res = 0.0;
    int start = args -> start; 
    int end   = args -> end; 
    float x   = args -> x;

    //TESiT
    //printf("start: %d\n end:%d\n x: %f\n",start, end, x);
    //TEST
    
    //Calculate the pratial sum in the given range
    for (int i = start; i < end; i++)
    {
        partial_res += powf((float)(-1),i)*powf(x,2*i+1)/(float)factorial(2*i+1);
        
        // TEST
        //if(i ==start)
            //printf ( "fact: %d \n",factorial(2*i+1)  );
        //TEST
    }
    
    pthread_mutex_lock(&mutex1);
    
    result += partial_res;
    //TESiT
    //printf("start: %d\n end:%d\n x: %f\n",start, end, x);
    //printf("partial: %f\n",partial_res);
    //TEST
    pthread_mutex_unlock(&mutex1); 
    pthread_exit(NULL);
}


main()
{
    assert(NUM_THREADS < ORDER);

    // Declare
    pthread_t thread_id[NUM_THREADS];
    pthread_attr_t attr;  
    int i, j;
    float x = M_PI;

    // Initizlize mutex and attributes
    pthread_mutex_init(&mutex1, NULL);
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    struct arg_struct args[NUM_THREADS];
    //Launch threads
    int len = ORDER / (NUM_THREADS+1);
    for(i=0; i < NUM_THREADS; i++)
    {
        // Calculate how much job each thread has to do
        args[i].start = i*len;
        args[i].end   = (i+1)*len;
        args[i].x     = x;
        pthread_create( &thread_id[i], &attr, sin_mcln, (void *)&args[i]);
    }

    // Sync threads
    for(j=0; j < NUM_THREADS; j++)
    {
       pthread_join( thread_id[j], NULL); 
    }
    
    //Calculate remainder that is smaller than len
    for (int i = NUM_THREADS*len; i < ORDER; i++)
    {
        result += powf(float(-1),i+1)*powf(x,2*i+2)/(float)factorial(2*i+2);
    }

    
    printf("Maclaurin Approximation of sin(%f) is %f \n",x, result);

    // Be nice and clean after yourself
    pthread_mutex_destroy(&mutex1);
    pthread_exit(NULL);    
}

