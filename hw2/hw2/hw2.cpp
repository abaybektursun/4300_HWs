// Abay Bektursun - 001172101
// Brea Lockhart - 
#include <iostream>
#include <thread>


//This Code follows C++11 Standards

//NOTE:
//CreateThread (with dword handle, etc) is specific to WinAPI, this implies writing non-portable code. Also, this API is quite old and more inconvenient to use.
//Therefore we are following best practices and using latest windows threads

// We are just going to create threads that will print their IDs
void call_from_thread(int thread_idx) {
	std::cout << "Thread: " << thread_idx << std::endl;
}

int main() {
	// In order to maximze the efficency we create as many threads as there are cores on the system 
	static const int concurrent_cores = std::thread::hardware_concurrency();;
	std::thread *t = new std::thread[concurrent_cores];

	std::cout << "Number of cores detected on the computer: " << concurrent_cores << std::endl;

	//Create the threads
	for (int i = 0; i < concurrent_cores; ++i) {
		t[i] = std::thread(call_from_thread, i);
	}

	

	//Syncronize threads, and stop them
	for (int i = 0; i < concurrent_cores; ++i) {
		t[i].join();
	}

	std::cout << "Done\n";

	return 0;
}